from django.shortcuts import redirect, render
from django.http import HttpResponse
from .models import Kegiatan, Partisipan
from .forms import KegiatanForm, PartisipanForm

# Create your views here.
def tambahKegiatan(request):
    data_kegiatan_list = Kegiatan.objects.all()
    form = KegiatanForm()
    if request.method == 'POST':
        forminput = KegiatanForm(request.POST)
        if forminput.is_valid():
            forminput.save()
            return render(request, 'tambah_kegiatan.html', {'form':form, 'status': 'success', 'data' : data_kegiatan_list})
        else:
            return render(request, 'tambah_kegiatan.html', {'form':form, 'status': 'failed', 'data' : data_kegiatan_list})
            
    else: #Kalo method GET
        current_data = Kegiatan.objects.all()
        return render(request, 'tambah_kegiatan.html', {'form':form, 'data':current_data})

def dataKegiatan(request):
    form = PartisipanForm();
    data_kegiatan_list = Kegiatan.objects.all()
    data_partisipan_list = Partisipan.objects.all()
    if request.method == 'POST':
        forminput = PartisipanForm(request.POST)
        if forminput.is_valid():
            forminput.save()
        return redirect(dataKegiatan)
    else:
        return render(request,'data_kegiatan.html', {'form':form, 'status': 'failed', 'dataPartisipan' : data_partisipan_list, 'dataKegiatan': data_kegiatan_list})
